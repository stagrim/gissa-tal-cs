﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GissaTal
{
	class Program
	{
		static void Main()
		{
			Random rand = new Random();
			int number = rand.Next(1, 101);
			int tries = 0;
			int[] guess = new int[2];

			do
			{
				Console.WriteLine("Enter a number between 1 to 100");

				string input = Console.ReadLine();
				guess = Expectation(input.Trim());

				if (guess[1] == 0)
				{
					Console.WriteLine("The number can't contain letters, please try again\n");
					continue;
				}
				else if (guess[1] == 1 || guess[0] < 1 || guess[0] > 100)
				{
					Console.WriteLine("Must be a number between 1 to 100, please try again\n");
					continue;
				}

				tries++;

				if (guess[0] > number)
				{
					Console.WriteLine("You guessed too high\n");
				}
				else if (guess[0] < number)
				{
					Console.WriteLine("You guessed too low\n");
				}
			} while (guess[0] != number);

			Console.WriteLine("\nCongratulations, you guessed " + guess[0] + " correctly after " + tries + " tries");
			Console.ReadKey();
		}
		static int[] Expectation(string input)
		{
			int[] output = new int[2];

			try
			{
				output[0] = int.Parse(input);
				output[1] = 2;
			}
			catch (System.FormatException)
			{
				output[0] = 0;
				output[1] = 0;
			}
			catch
			{
				output[0] = 0;
				output[1] = 1;
			}

			return output;
		}
	}
}
