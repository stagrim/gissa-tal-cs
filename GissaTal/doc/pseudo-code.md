# Pseudokod

1. Skapa String input
2. Skapa u32 number
3. Skapa u32 guess
4. Skapa u32 tries
5. Generera ett tal mellan 1-100, lagra i number
6. Be användaren att mata in ett tal mellan 1-100, lagra sedan i input
7. Konvertera input till u32 och lagra i guess
8. Om guess är större eller mindre än 1-100

    8.1 Skriv talet är för stort / för litet

    8.2 Gå tillbaka till 5

9. Om guess är lika med number

    9.1 Öka tries med ett

    9.2 Skriv att användaren gissade rätt

    9.3 Skriv antalet försök lagrade i tries

    9.4 Avsluta programmet

10. Om guess är större än number

    10.1 Öka tries med ett

    10.2 Skriv att användaren gissade för högt

    10.3 Gå tillbaka till 5

11. Om guess är större än number

    11.1 Öka tries med ett

    11.2 Skriv att användaren gissade för lågt

    11.3 Gå tillbaka till 5
